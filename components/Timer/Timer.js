import React, { Component } from 'react';
import ReactInterval from 'react-interval';
import noop from 'lodash/noop';

import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';

import { previewSeconds } from '/utils';

class Timer extends Component {
  constructor(props) {
    super(props);
    const startTime = new Date().getSeconds();

    this.state = {
      pause: false,
      roundAm: 0,
      startTime,
      endTime: startTime + props.roundTime,
      rest: props.rest
    }

    this.decreaseTime = this.decreaseTime.bind(this);
    this.togglePause = this.togglePause.bind(this);
  }

  decreaseTime() {
    const { startTime, endTime, rest, roundAm } = this.state;

    if (endTime - startTime > 0) {
      this.setState({
        startTime: startTime + 1
      });
    } else {
      const startTime = new Date().getSeconds();
      const deltaTime = rest ? this.props.roundTime : this.props.restTime;

      this.props.changeRound(roundAm);
      this.setState({
        startTime,
        endTime: startTime + deltaTime - 1,
        rest: !rest,
        roundAm: roundAm + (rest ? 0 : 1)
      });
    }
  }

  togglePause() {
    const { pause } = this.state;
    this.setState({pause: !pause});
    this.props.togglePause(!pause);
  }

  reset = () => {
    const startTime = new Date().getSeconds();

    this.setState({
          pause: false,
          roundAm: 0,
          startTime,
          endTime: startTime + this.props.roundTime,
          rest: this.props.rest
        })
  }

  render() {
    const { pause, rest, roundAm } = this.state;
    const reactIntervalProps = {
      timeout: 1000,
      enabled: true,
      callback: this.decreaseTime
    };
    const restOrWorkClass = rest ? styles.rest : styles.work;

    return (
      <View style={styles.container}>
        <View style={styles.timer}>
          <Text style={[styles.phraseContainer, restOrWorkClass]}>
            {rest ? 'rest time is passing!' : 'keep going! you can do it!'}
          </Text>
          <Text style={[styles.clockFace, restOrWorkClass]}>
            {previewSeconds(this.state.endTime - this.state.startTime)}
          </Text>
        </View>
        <View>
          <Text style={styles.roundTextHeading}>
            Finished rounds:
          </Text>
          <Text style={styles.roundText}>
            {roundAm}
          </Text>
        </View>
        <View style={styles.controls}>
          <TouchableHighlight onPress={this.togglePause} style={[styles.button, styles.pauseButton]}>
            <Text style={styles.buttonText}>
              {pause ? 'Continue' : 'Pause'}
            </Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={this.reset} style={[styles.button, styles.turnOffButton]}>
            <Text style={styles.buttonText}>
              Reset
            </Text>
          </TouchableHighlight>
        </View>

        <ReactInterval timeout={1000} enabled={!pause} callback={this.decreaseTime} />
      </View>
    );
  }
}

Timer.defaultProps = {
  changeRound: noop,
  turnOff: noop,
  togglePause: noop,
  roundTime: 180,
  restTime: 60,
  rest: false
}

const styles = StyleSheet.create({
  container: {
    width: 350,
    flex: 1,
    justifyContent: 'center'
  },
  roundText: {
    textAlign: 'center',
    fontSize: 150,
    color: 'green'
  },
  timer: {
    flex: 2
  },
  turnOffButton: {
    backgroundColor: 'red',
  },
  pauseButton: {
    backgroundColor: 'orange'
  },
  roundTextHeading: {
    fontSize: 30,
    color: 'green'
  },
  button: {
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    fontSize: 22
  },
  controls: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rest: {
    color: 'green',
  },
  work: {
    color: 'orange',
  },
  phraseContainer: {
    textAlign: 'center',
    fontSize: 30,
  },
  clockFace: {
    flex: 4,
    fontSize: 80,
    textAlign: 'center',
    margin: 10,
  }
});

export default Timer;
