import React, { Component } from 'react';
import ReactInterval from 'react-interval';
import noop from 'lodash/noop';

import {
  AppRegistry,
  View,
  Text,
  StyleSheet
} from 'react-native';

import Timer from './components/Timer';

class roundTimer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.timerContainer}>
          <Timer />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 22,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  timerContainer: {
    flex: 9
  },
  footerElement: {
    flex: 1,
  },
  footerElementWider: {
    flex: 2,
  }
});

AppRegistry.registerComponent('roundTimer', () => roundTimer);
