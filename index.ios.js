import React, { Component } from 'react';
import ReactInterval from 'react-interval';
import noop from 'lodash/noop';

import {
  AppRegistry,
  View,
  Text,
  StyleSheet
} from 'react-native';

import Timer from './components/Timer';

class roundTimer extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.heading}>
          <Text>some new text</Text>
        </View>
        <View style={styles.timerContainer}>
	        <Timer />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  heading: {
  	flex: 1,
  	marginTop: 22,
  },
  timerContainer: {
  	flex: 9
  },
  footer: {
  	flex: 1,
  	padding: 10,
  	flexDirection: 'row',
  	justifyContent: 'center',
    alignItems: 'center',
  	backgroundColor: '#FF0000',
  	justifyContent: 'space-between',
  },
  footerElement: {
  	flex: 1,
  },
  footerElementWider: {
  	flex: 2,
  }
});

AppRegistry.registerComponent('roundTimer', () => roundTimer);
