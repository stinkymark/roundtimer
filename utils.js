export function previewSeconds(seconds) {
	const minutes = Math.floor(seconds/60);
	const restSeconds = seconds - minutes*60;
	return `${minutes || '00'}:${ Math.floor(restSeconds/10) ? `${restSeconds}` : `0${restSeconds}` }`;
}
